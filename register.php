<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 8:23
 */

    // creating header
    include("view/visual.php");
    getHeader("Registrace");
?>

<?php
    // managing site control
    include("controllers/actions.class.php");
    $SiteControler = new actions();
    $SiteControler->checkActions();
?>

    <h3>Registrovat nového uživatele</h3>

<?php
    if($SiteControler->getPDOControler()->isLogged()){
        // display for logged only
?>

    <form method="POST" action="" >
        <p>Již jste přihlášeni!</p>
        <p>Pokud si chcete vytvořit nový účet, musíte se nejdříve odhlásit.</p>

        <input type="hidden" name="action" value="logout">
        <input type="submit" value="Odhlásit">
    </form>

<?php
    } else {
        // display for not logged only
?>

        <!-- form with table for better alignment options-->
        <form autocomplete="off" method="post" action="">

            <!-- row contains text field-->
            <div class="alignmenthelpers">
                <label for="nick">
                    Přezdívka:
                </label>
            </div>
            <input type="text" id="nick" name="nick" maxlength="15" required>

            <!-- row contains text field for password-->
            <div class="alignmenthelpers">
                <label for="pass">
                    Heslo:
                </label>
            </div>
            <input type="password" id="pass" name="pass" maxlength="20" required>

            <!-- row contains text field for password-->
            <div class="alignmenthelpers">
                <label for="passretry">
                    Potvrzení hesla:
                </label>
            </div>
            <input type="password" id="passretry" name="passretry" maxlength="20" required>

            <!-- row contains text field for email-->
            <div class="alignmenthelpers">
                <label for="email">
                    Email:
                </label>
            </div>
            <input type="email" id="email" name="email" maxlength="50" required>

            <!-- row contains send button-->
            <output name="error_display"></output>
            <input type="hidden" name="action" value="register">
            <input type="submit" name="register" value="Registrovat">
        </form>

        <!-- additional links-->
        <div id="bubblelinks">
            <a href="login.php">Máte již účet?</a>
        </div>

<?php
    }
?>

<?php
    // creating footer
    getFooter();
?>
<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 1. 12. 2018
 * Time: 22:55
 */

// managing database control
include("model/db_control.class.php");

class actions{
    private $PDOControler;

    public function __construct(){
        // managing database control
        $this->PDOControler = new db_control();
    }

    public function checkActions(){
        if((isset($_REQUEST["action"])) && ($_REQUEST["action"] == "login")){
            if(!$this->PDOControler->logIn($_POST['nick'],$_POST['pass'])){
                echo "<p id='error_display'>Přihlášení se nezdařilo!<br>Zadán neplatný uživatel nebo heslo!</p>";
            }
        } else if((isset($_REQUEST["action"])) && ($_REQUEST["action"] == "logout")){
            setcookie("rating", "", 1, "/");
            $this->PDOControler->logOut();
        } else if((isset($_REQUEST["action"])) && ($_REQUEST["action"] == "add_article")){
            if(($this->PDOControler->isLogged()) && ($_SESSION["user"]["rights"] == "Autor")){
                $nick = $_SESSION["user"]["nick"];


                // checking for errors
                if($_FILES['filepdf']['error'] > 0){
                    echo "<p id='error_display'>Nastala chyba během uploadu!</p>";
                    die();
                }

                // checking filetype
                if($_FILES['filepdf']['type'] != 'application/pdf'){
                    echo "<p id='error_display'>Formát souboru není podporován!</p>";
                    die();
                }

                // checking filesize
                if($_FILES['filepdf']['size'] > 500000){
                    echo "<p id='error_display'>Velikost souboru přesahuje maximální povolenou velikost!</p>";
                    die();
                }

                // checking if the file exists
                if(file_exists('upload/' . $_FILES['filepdf']['name'])){
                    echo "<p id='error_display'>Soubor s tímto jménem již existuje!</p>";
                    die();
                }

                // uploading file
                if(!move_uploaded_file($_FILES['filepdf']['tmp_name'], 'upload/' . $_FILES['filepdf']['name'])){
                    echo "<p id='error_display'>Nastala chyba během uploadu!</p>";
                    die();
                }

                $this->PDOControler->addArticle($_POST["name"],$_POST["content"],'upload/' . $_FILES['filepdf']['name'],$nick);
            } else {
                echo "<p id='error_display'>Tato operace může být provedena pouze přihlášeným uživatelem se statusem autora!</p>";
            }
        } else if((isset($_REQUEST["action"])) && ($_REQUEST["action"] == "register")){
            if($_POST["pass"] == $_POST["passretry"]){
                if($this->PDOControler->getUser($_POST["nick"]) == null){
                    $this->PDOControler->newUser($_POST["nick"], $_POST["pass"], $_POST["email"]);
                    $this->PDOControler->logIn($_POST["nick"],$_POST["pass"]);
                } else {
                    echo "<p id='error_display'>Uživatel s touto přezdívkou již existuje!</p>";
                }
            } else {
                echo "<p id='error_display'>Zadaná hesla se neshodují!</p>";
            }
        } else if((isset($_REQUEST["action"])) && ($_REQUEST["action"] == "add_rating")){
            $execuded = $this->PDOControler->addRating($_COOKIE['rating'],$_POST['theme'], $_POST['language'], $_POST['utility'], $_POST['notes']);
            if($execuded == null){
                echo "<p id='error_display'>Hodnocnení nemohlo být přidáno!</p>";
            } else {
                echo "<p id='error_display'>Článek byl úspěšně ohodnocen!</p>";
            }
            setcookie("rating", "", 1, "/");
        } else if((isset($_REQUEST["action"])) && (isset($_REQUEST["article_id"])) && ($_REQUEST["action"] == "publish")){
            $article = $_REQUEST["article_id"];
            $executed = $this->PDOControler->publishArticle($article);
            if ($executed == null) {
                echo "<p id='error_display'>Článek nemohl být zveřejněn!</p>";
            } else {
                echo "<p id='error_display'>Článek byl úspěšně zveřejněn!</p>";
            }
        } else if((isset($_REQUEST["action"])) && (isset($_REQUEST["article_id"])) && ($_REQUEST["action"] == "add_rater")){
            $article = $_REQUEST["article_id"];
            $executed = $this->PDOControler->createRating($article, $_POST['rater_select']);
            if ($executed == null) {
                 echo "<p id='error_display'>Nepodařilo se přidělit článek recenzentovi!</p>";
            } else {
                 echo "<p id='error_display'>Článek byl úspěšně přidělen!</p>";
            }
        } else if((isset($_REQUEST["action"])) && ($_REQUEST["action"] == "new_rating")){
            setcookie("rating", $_POST['key_id'], time() + (60 * 5), "/");
            header("Location: newrating.php");
            die();
        } else if((isset($_REQUEST["action"])) && (isset($_REQUEST["key_nick"])) && ($_REQUEST["action"] == "remove")){
            $user = $_REQUEST["key_nick"];
            $executed = $this->PDOControler->removeUser($user);
            if($executed == null){
                echo "<p id='error_display'>Uživatel $user nemohl být odstraněn!</p>";
            } else {
                echo "<p id='error_display'>Uživatel $user byl úspěšně odstraněn!</p>";
            }
        } else if((isset($_REQUEST["action"])) && (isset($_REQUEST["key_nick"])) && ($_REQUEST["action"] == "change")){
            $user = $_REQUEST["key_nick"];
            $right = $_POST["select_rights"];
            $executed = $this->PDOControler->changeRights($user,$right);
            if($executed == null){
                echo "<p id='error_display'>Práva užvatele $user nemohla být změněna!</p>";
            } else {
                echo "<p id='error_display'>Práva užvatele $user byla úspěšně změněna!</p>";
            }
        }
    }

    public function getPDOControler(){
        return $this->PDOControler;
    }
}
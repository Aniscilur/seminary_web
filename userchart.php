<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 11:38
 */

    // creating header
    include("view/visual.php");
    getHeader("Seznam uživatelů");
?>

<?php
    // managing site control
    include("controllers/actions.class.php");
    $SiteControler = new actions();
    $SiteControler->checkActions();
?>

    <h3>Seznam uživatelů</h3>

<?php
    if($SiteControler->getPDOControler()->isLogged() && $_SESSION["user"]["rights"] == "Admin"){
    // display for logged admins only
?>


<?php
        $users = $SiteControler->getPDOControler()->getAllUsers();
        if($users == null){
            echo "<p id='error_display'>Databáze je prázdná nebo obsahuje pouze adminy!</p>";
        } else {
            echo " <!-- table with users who are not admins-->
                   <table>
                        <tr>
                            <th>Přezdívka</th>
                            <th>Email</th>
                            <th>Práva</th>
                        </tr>";

            foreach($users as $key){
                echo "<tr>
                        <td>$key[nick]</td>
                        <td>$key[email]</td>
                                
                        <!-- allows changing rights -->
                        <td>
                            <form method='POST' action='' id='no_bubble' >
                                <input type='hidden' name='key_nick' value='$key[nick]'>
                                <input type='hidden' name='action' value='change'>
                                <select name='select_rights'>
                                    <option value='Autor' ".(($key['rights'] == 'Autor')?"selected":"" ).">Autor</option>
                                    <option value='Recenzent' ".(($key['rights'] == 'Recenzent')?"selected":"" ).">Recenzent</option>
                                    <option value='Admin'>Admin</option>
                                </select>  
                            <input type='submit' value='Změnit'>
                            </form>                           
                        </td>
                                
                        <!-- allows deleting users-->
                        <td>
                            <form method='POST' action='' id='no_bubble' >
                                <input type='hidden' name='key_nick' value='$key[nick]'>
                                <input type='hidden' name='action' value='remove'>
                                <input type='submit' value='Odstranit'>
                            </form>
                        </td>
                      </tr>";
            }

            echo "</table>";
        }
?>

        <!-- additional links-->
        <div id="bubblelinks">
            <a href="login.php">Zpět na osobní profil</a>
        </div>

<?php
        } else {
?>

        <p id='error_display'>Tyto stránky jsou přístupné pouze přihlášeným adminům!</p>

<?php
        }
?>

<?php
    // creating footer
    getFooter();
?>

<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 8:23
 */

    // creating header
    include("view/visual.php");
    getHeader("Příspěvky");
?>

<?php
    // managing database control
    include("model/db_control.class.php");
    $PDOControler = new db_control();
?>

<?php
    //if possible displays accepted articles(newest is up), if not display error msg
    $articles = $PDOControler->getAcceptedArticles();
    if($articles == null){
        echo "<p id='error_display'>Nebyl nalezen žádný příspěvek!</p>";
    } else {
        foreach($articles as $key){
            echo "<h3>$key[name]</h3><p>$key[context] - od <i>$key[user_nick]</i><br><a href='$key[file]'>Stáhnout soubor</a></p><hr>";
        }
    }
?>

<?php
// creating footer
getFooter();
?>

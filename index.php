<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 8:23
 */

    // creating header
    include("view/visual.php");
    getHeader("Úvod");
?>

<!-- the foreword-->
<h3>Úvodní slovo</h3>
<p>Vítejte na stránkách <span>Gamer's Paradise</span>! Tyto stránky slouží k sdílení článků důležitých pro každého počítačového
    nadšence. Hledáte informace o nově vydaných hrách nebo snad informace o nových technologiích?
    To vše najdete na těchto stránkách.</p>

<h3>Jak přispívat?</h3>
<p>Pokud se chcete přidat ke kolektivu Gamer's Paradise a přidávat příspěvky, není nic jednodušího. Jediné,
    co musíte udělat, je vytvořit si účet, a automaticky dostanete statut autora. Vámi psané příspěvky se ovšem
    neobjeví ihned, musí být nejprve ohodnoceny a schváleny. Bude-li Váš článek vyhodnocen, jako nepřínosný, nebo
    jakkoliv urážlivý, bude Vám vrácen k přepracování. Abyste zamezili vrácení článku, snažte se udržet spisovný jazyk
    a neutrální, nebo aspoň neurážlivý tón Vašeho článku. Netřeba snad podotýkat, že články by měly být rozděleny
    do odstavců, a obsahují-li obrázky, musí být uveden jejich autor. Jakékoliv plagiátorství a omezení autorských
    práv nebude tolerováno.</p>

<?php
// creating footer
    getFooter();
?>

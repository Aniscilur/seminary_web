<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 9:04
 */

    // creating header
    include("view/visual.php");
    getHeader("Přihlášení");
?>

<?php
    // managing site control
    include("controllers/actions.class.php");
    $SiteControler = new actions();
    $SiteControler->checkActions();
?>

    <h3>Přihlášení uživatele</h3>

<?php
    if($SiteControler->getPDOControler()->isLogged()){
        // display for logged only
?>
        <!-- form with user info-->
        <form method="POST" action="" >
            <div class="alignmenthelpers">
                <label>
                    Přezdívka:
                </label>
            </div>
            <?php echo "<p>".$_SESSION["user"]["nick"]."</p>" ?>

            <div class="alignmenthelpers">
                <label>
                    Práva:
                </label>
            </div>
            <?php echo "<p>".$_SESSION["user"]["rights"]."</p>" ?>

            <input type="hidden" name="action" value="logout">
            <input type="submit" value="Odhlásit">
        </form>

        <!-- additional links-->
        <div id="bubblelinks">
            <!-- displays proper anchor -->
            <?php
                if($_SESSION["user"]["rights"] == "Autor") {
            ?>

                <a href="newarticle.php" title="Přidat nový článek">Nový článek</a>

            <?php
                } else if($_SESSION["user"]["rights"] == "Recenzent"){
            ?>
                <a href="ratingchart.php" title="Zobrazit má hodnocení">Seznam přiřazených hodnocení</a>

            <?php
                } else {
            ?>
                <a href="userchart.php" title="Zobrazit uživatele">Seznam uživatelů</a><br>
                <a href="rating_to_article.php" title="Zobrazit dosud nepřijaté články">Seznam článků k přijetí</a>

            <?php
            }
            ?>
        </div>
<?php
    } else {
        // display for not logged only
?>

        <!-- form with divs for better alignment options-->
        <form autocomplete="off" method="post" action="">
            <input type="hidden" name="action" value="login">

            <!-- row contains text field-->
            <div class="alignmenthelpers">
                <label for="nick">
                    Přezdívka:
                </label>
            </div>
            <input type="text" id="nick" name="nick" maxlength="15" required>
            <br>

            <!-- row contains text field for passwords-->
            <div class="alignmenthelpers">
                <label for="pass">
                    Heslo:
                </label>
            </div>
            <input type="password" id="pass" name="pass" maxlength="20" required>
            <br>

            <!-- row contains send button-->
            <input type="submit" value="Příhlásit">
        </form>

        <!-- additional links-->
        <div id="bubblelinks">
            <a href="register.php">Nemáte ještě účet?</a>
        </div>

<?php
    }
?>

<?php
    // creating footer
    getFooter();
?>
<?php
    /**
     * Created by PhpStorm.
     * User: Ondřej
     * Date: 14. 11. 2018
     * Time: 15:28
     */

    // creating header
    include("view/visual.php");
    getHeader("Seznam článků k přijetí");
?>

<?php
    // managing site control
    include("controllers/actions.class.php");
    $SiteControler = new actions();
    $SiteControler->checkActions();
?>

    <h3>Seznam článků k přijetí</h3>

<?php
    if($SiteControler->getPDOControler()->isLogged() && $_SESSION["user"]["rights"] == "Admin"){
        // display for logged admins only
?>

<?php
        $articles = $SiteControler->getPDOControler()->getAllNotAcceptedArticles();
        if($articles == null){
            echo "<p id='error_display'>Žádný článek nečeká na přijmutí!</p>";
        } else {
            echo " <!-- table with users who are not admins-->
                   <table>
                        <tr>
                            <th rowspan='2'>Název</th>
                            <th rowspan='2'>Autor</th>
                            <th colspan='5'>Recenze</th>                            
                        </tr>
                        <tr>
                            <th>Recenzent</th>
                            <th>Téma</th>
                            <th>Jazyk</th>
                            <th>Dopad</th>               
                            <th>Suma</th>
                        </tr>";

            foreach($articles as $key) {
                echo "<tr>
                        <td rowspan='3'>$key[name]</td>
                        <td rowspan='3'>$key[user_nick]</td>";

                $ratings = $SiteControler->getPDOControler()->getArticleRatings($key['id_article']);
                if(count($ratings) < 1){
                    echo "<td colspan='5'>
                            <form method='POST' action='' id='no_bubble' >
                                <input type='hidden' name='article_id' value='$key[id_article]'>
                                <input type='hidden' name='action' value='add_rater'>
                                <select name='rater_select'>";
                    echo            $SiteControler->getPDOControler()->getUserOptions();
                    echo       "</select>
                                <input type='submit' value='Přiřadit'>
                            </form>  
                          </td>";
                } else {
                    $name = $ratings[0]['user_nick'];
                    $theme = $ratings[0]['theme'];
                    $language = $ratings[0]['language'];
                    $impact = $ratings[0]['impact'];
                    $suma = $theme + $language + $impact;

                    echo "<td>$name</td>
                          <td>$theme</td>
                          <td>$language</td>
                          <td>$impact</td>
                          <td>$suma</td>";
                 }

                echo "      <td rowspan='3'>  
                            <form method='POST' action='' id='no_bubble' >
                                <input type='hidden' name='article_id' value='$key[id_article]'>
                                <input type='hidden' name='action' value='publish'>
                                <input type='submit' value='Vydat článek'>
                            </form>
                        </td>
                      </tr>
                      <tr>";

                if(count($ratings) < 2){
                    echo "<td colspan='5'>
                            <form method='POST' action='' id='no_bubble' >
                                <input type='hidden' name='article_id' value='$key[id_article]'>
                                <input type='hidden' name='action' value='add_rater'>
                                <select name='rater_select'>";
                    echo            $SiteControler->getPDOControler()->getUserOptions();
                    echo       "</select>
                                <input type='submit' value='Přiřadit'>
                            </form>  
                          </td>
                      </tr>";
                } else {
                    $name = $ratings[1]['user_nick'];
                    $theme = $ratings[1]['theme'];
                    $language = $ratings[1]['language'];
                    $impact = $ratings[1]['impact'];
                    $suma = $theme + $language + $impact;

                    echo "<td>$name</td>
                          <td>$theme</td>
                          <td>$language</td>
                          <td>$impact</td>
                          <td>$suma</td>
                          </tr>";
                 }

                if(count($ratings) < 3){
                    echo "<td colspan='5'>
                            <form method='POST' action='' id='no_bubble' >
                                <input type='hidden' name='article_id' value='$key[id_article]'>
                                <input type='hidden' name='action' value='add_rater'>
                                <select name='rater_select'>";
                    echo            $SiteControler->getPDOControler()->getUserOptions();
                    echo       "</select>
                                <input type='submit' value='Přiřadit'>
                            </form>  
                          </td>
                      </tr>";
                } else {$name = $ratings[2]['user_nick'];
                    $theme = $ratings[2]['theme'];
                    $language = $ratings[2]['language'];
                    $impact = $ratings[2]['impact'];
                    $suma = $theme + $language + $impact;

                    echo "<td>$name</td>
                          <td>$theme</td>
                          <td>$language</td>
                          <td>$impact</td>
                          <td>$suma</td>
                          </tr>";
                }
            }
            echo "</table>";
        }
?>

        <!-- additional links-->
        <div id="bubblelinks">
            <a href="login.php">Zpět na osobní profil</a>
        </div>

<?php
    } else {
?>

        <p id='error_display'>Tyto stránky jsou přístupné pouze přihlášeným adminům!</p>

<?php
    }
?>

<?php
    // creating footer
    getFooter();
?>
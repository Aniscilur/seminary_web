-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Ned 18. lis 2018, 13:45
-- Verze serveru: 5.7.14
-- Verze PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `web_seminary`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `article`
--

CREATE TABLE `article` (
  `id_article` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `context` varchar(300) DEFAULT NULL,
  `file` varchar(45) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `user_nick` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `article`
--

INSERT INTO `article` (`id_article`, `name`, `context`, `file`, `status`, `user_nick`) VALUES
(1, 'Try01', 'Lorem ipsum dolor sit amet quad nemo tenebris de lone', 'upload/tryfile_01.pdf', 'accepted', 'Olivier'),
(3, 'adwawdaw a', 'adwpwdapodw powd awd opawj dap wdpoa wdopawd apow apow djap  dawdadadw', 'upload/tryfile_02.pdf', 'unaccepted', 'Olivier'),
(8, 'Try 05', 'This is try', 'upload/tryfile_03.pdf', 'unaccepted', 'Olivier'),
(9, 'Try 06', 'This is also try', 'upload/tryfile_04.pdf', 'accepted', 'Olivier'),
(10, 'Try 08', 'This is last try', 'upload/tryfile_05.pdf', 'unaccepted', 'Olivier'),
(11, 'Valar Morgulis 02', 'This is try as all the others. But this one may perish... or not... whatever', 'upload/tryfile_06.pdf', 'unaccepted', 'Ignacius'),
(12, 'upload try', 'will the file transfer now?', 'upload/tryfile_07.pdf', 'unaccepted', 'Olivier');

-- --------------------------------------------------------

--
-- Struktura tabulky `rating`
--

CREATE TABLE `rating` (
  `rating_number` int(11) NOT NULL,
  `theme` int(11) DEFAULT NULL,
  `language` int(11) DEFAULT NULL,
  `impact` int(11) DEFAULT NULL,
  `notes` varchar(300) DEFAULT NULL,
  `user_nick` varchar(20) NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `rating`
--

INSERT INTO `rating` (`rating_number`, `theme`, `language`, `impact`, `notes`, `user_nick`, `article_id`) VALUES
(1, 3, 4, 5, 'Looking goood', 'LostLamb', 8),
(2, NULL, NULL, NULL, NULL, 'UrsaTheWitch', 8),
(3, NULL, NULL, NULL, NULL, 'UrsaTheWitch', 3),
(4, 1, 3, 4, 'Improving', 'LostLamb', 3),
(5, NULL, NULL, NULL, NULL, 'UrsaTheWitch', 11);

-- --------------------------------------------------------

--
-- Struktura tabulky `user`
--

CREATE TABLE `user` (
  `nick` varchar(20) NOT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `rights` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `user`
--

INSERT INTO `user` (`nick`, `pass`, `email`, `rights`) VALUES
('Aniscilur', 'nomajorpass', 'admin@admin.ad', 'Admin'),
('Ignacius', 'nopass', 'halo@ignus.mort', 'Autor'),
('LostLamb', 'nopass', 'lost@lama.cy', 'Recenzent'),
('Olivier', 'nopass', 'wthisthis@asap.tr', 'Autor'),
('UrsaTheWitch', 'nopass', 'ugly@witch.wz', 'Recenzent');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id_article`),
  ADD KEY `fk_article_user1_idx` (`user_nick`);

--
-- Klíče pro tabulku `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`rating_number`),
  ADD KEY `fk_rating_user1_idx` (`user_nick`),
  ADD KEY `fk_rating_article1_idx` (`article_id`);

--
-- Klíče pro tabulku `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`nick`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `article`
--
ALTER TABLE `article`
  MODIFY `id_article` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pro tabulku `rating`
--
ALTER TABLE `rating`
  MODIFY `rating_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_article_user1` FOREIGN KEY (`user_nick`) REFERENCES `user` (`nick`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `fk_rating_article1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id_article`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rating_user1` FOREIGN KEY (`user_nick`) REFERENCES `user` (`nick`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

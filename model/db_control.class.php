<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 9:04
 */

include_once("db_connect.inc.php");

class db_control{
    private $db;

    /**
     * db_control constructor.
     * connects to database and start sessions
     */
    public function __construct(){
        global $db_server, $db_name, $db_user, $db_pass;

        // setting connection, starting sessions
        $this->db = new PDO("mysql:host=$db_server;dbname=$db_name", $db_user, $db_pass);
        session_start();
    }

    /**
     * checked if anyone is logged
     * @return bool true when user is logged, false otherwise
     */
    public function isLogged(){
        return isset($_SESSION["user"]);
    }

    /**
     * logs user
     * @param $login filled nick of user
     * @param $pass filled password
     * @return bool if user wasn't logged
     */
    public function logIn($login, $pass){
        $login = htmlspecialchars($login);
        $pass = htmlspecialchars($pass);
        if(!$this->checkPass($login,$pass)){
            return false;
        } else {
            $_SESSION["user"] = $this->getUser($login);
            return true;
        }
    }

    /**
     * logs out by unseting sessions
     */
    public function logOut(){
        session_unset($_SESSION["user"]);
    }

    /**
     * @param $login name of user to log
     * @param $pass password to account
     * @return bool returns true if passwords matches
     * returns false otherwise
     */
    public function checkPass($login, $pass){
        $login = htmlspecialchars($login);
        $pass = htmlspecialchars($pass);
        $user = $this->getUser($login);
        if($user == null){
            return false;
        }
        return $user["pass"] == $pass;
    }

    /**
     * this creates new user
     * @param $nick nick of user to be added
     * @param $pass password of user
     * @param $email email of user
     * @return bool returns true when order were executed
     */
    public function newUser($nick, $pass, $email){
        $nick = htmlspecialchars($nick);
        $pass = htmlspecialchars($pass);
        $email = htmlspecialchars($email);
        $order = "INSERT INTO user(nick,pass,email,rights)
                  VALUES (?,?,?,'Autor')";
        $order = $this->db->prepare($order);
        $executed = $order->execute(array($nick, $pass,$email));
        return $executed;
    }

    /**
     * this function returns user based on his nick
     * @param $nick name of user searched
     * @return null if user doesn't exist or something collapses
     */
    public function getUser($nick){
        $nick = htmlspecialchars($nick);
        $sql = "SELECT * FROM user
                  WHERE user.nick = :nick";

        $order = $this->db->prepare($sql);
        $order->bindParam(':nick',$nick);
        $order->execute();
        $executed = $order->fetchAll();

        if( ($executed == null) || (count($executed)) == 0){
            return null;
        } else {
            return $executed[0];
        }
    }

    /**
     * this function convert object into usable data
     * @param $object made by arrays of data
     * @return mixed is an array of required data
     */
    private function resultObjectToArray($object){
        return $object->fetchAll();
    }

    /**
     * this function executes MySQL orders
     * @param $order MySQL to be executed
     * @return bool|null|PDOStatement is the order were executed returns true or the required data object
     * null is returned otherwise
     */
    private function executeQuery($order){
        $executed = $this->db->query($order);
        if ($executed) {
            return $executed;
        } else {
            $error = $this->db->errorInfo();
            echo $error[2];
            return null;
        }
    }

    /**
     * this function returns all accepted articles
     * @return bool|mixed|null|PDOStatement if there is at least one accepted article returns true
     * if there isn't even one or if the order cound't be executed returns null
     */
    public function getAcceptedArticles(){
        $order = "SELECT * FROM article
                  WHERE article.status = 'accepted'
                  ORDER BY id_article DESC";

        $executed = $this->executeQuery($order);
        $executed = $this->resultObjectToArray($executed);

        if((count($executed)) == 0 || ($executed == null)){
            return null;
        } else {
            return $executed;
        }
    }

    /**
     * returns all users who are not admins
     * @return bool|mixed|null|PDOStatement if there is at least one "no-admin" return data
     * if there is none return null
     */
    public function getAllUsers(){
        $order = "SELECT * FROM user
                  WHERE user.rights != 'Admin'";

        $executed = $this->executeQuery($order);
        $executed = $this->resultObjectToArray($executed);

        if((count($executed)) == 0 || ($executed == null)){
            return null;
        } else {
            return $executed;
        }
    }

    /**
     * this function deletes user with given nick
     * @param $nick nick of the use who is to be deleted
     * @return null when user couldn't be deleted
     * returns true otherwise
     */
    public function removeUser($nick){
        $nick = htmlspecialchars($nick);
        $order = "DELETE FROM user
                  WHERE user.nick = :nick";

        $arguments = array(':nick' => $nick);
        $order = $this->db->prepare($order);

        if(!($executed = $order->execute($arguments))){
            $executed = null;
        }

        if($executed == null){
            return null;
        } else {
            return $executed;
        }
    }

    /**
     * this function changes rights of given user
     * @param $nick nick of user whose,rights are bout to be changed
     * @param $rights rights which are bout to be given to user
     * @return bool|null|PDOStatement is true when rights coul have been changed
     * returns null otherwise
     */
    public function changeRights($nick, $rights){
        $nick = htmlspecialchars($nick);
        $rights = htmlspecialchars($rights);
        if($rights!=""){
            $order = "UPDATE user
                      SET rights=:rights
                      WHERE user.nick=:nick";

            $arguments = array(':nick' => $nick, ':rights' => $rights);
            $order = $this->db->prepare($order);

            if(!($executed = $order->execute($arguments))){
                return null;
            } else {
                return $executed;
            }
        } else {
            return null;
        }
    }

    /**
     * this adds new article
     * @param $name name of the article
     * @param $context context of the article
     * @param $file artcile file
     * @param $user_nick author of article
     * @return bool is true if added
     * false otherwise
     */
    public function addArticle($name, $context, $file, $user_nick){
        $name = htmlspecialchars($name);
        $context = htmlspecialchars($context);
        $file = htmlspecialchars($file);
        $user_nick = htmlspecialchars($user_nick);
        $order = "INSERT INTO article(name, context, file, status, user_nick)
                  VALUES (:name,:context, :file,'unaccepted',
                  (SELECT nick FROM user WHERE user.nick=:user_nick))";

        $arguments = array(':name' => $name, ':context' => $context, ':file' => $file, ':user_nick' => $user_nick);
        $order = $this->db->prepare($order);
        $order->execute($arguments);
        $executed = $order->fetchAll();

        if($executed){
            echo "<p id='error_display'>Soubor nemohl být registrován!</p>";
        } else {
            echo "<p id='error_display'>Soubor byl úspěšně registrován!</p>";
        }
        return $executed;
    }

    /**
     * this function returns artices which should user rate
     * @param $user nick of user, who should rate the article
     * @return null when no such article exist
     * return true otherwise
     */
    public function getArticlesToRate($user){
        $user = htmlspecialchars($user);
        $order = "SELECT * FROM article, rating
                  WHERE article.id_article = rating.article_id
                  AND article.status = 'unaccepted' 
                  AND rating.user_nick = (SELECT user.nick
                  FROM user
                  WHERE user.nick = :user)";

        $arguments = array(':user' => $user);
        $order = $this->db->prepare($order);
        $order->execute($arguments);
        $executed = $order->fetchAll();

        if(!($executed) || (count($executed)) == 0){
            return null;
        } else {
            return $executed;
        }
    }

    /**
     * this function returns all unaccepted articles
     * @return bool|mixed|null|PDOStatement true is returned when there is at least one unaccepted article
     * return null otherwise
     */
    public function getAllNotAcceptedArticles(){
        $order = "SELECT id_article,name,status,user_nick FROM article
                  WHERE article.status = 'unaccepted'";

        $executed = $this->executeQuery($order);
        $executed = $this->resultObjectToArray($executed);

        if((count($executed)) == 0 || ($executed == null)){
            return null;
        } else {
            return $executed;
        }
    }

    /**
     * this function makes artice visible even for authors and unlogged users
     * @param $article article to be published
     * @return null if the article couldn't been published
     * true is returned otherwise
     */
    public function publishArticle($article){
        $article = $article + 0;
        if($article!=""){
            $order = "UPDATE article
                      SET status='accepted'
                      WHERE article.id_article = :article";

            $arguments = array(':article' => $article);
            $order= $this->db->prepare($order);

            if(!($executed = $order->execute($arguments))){
                $executed = null;
            }

            if($executed == null){
                return null;
            } else {
                return $executed;
            }
        } else {
            return null;
        }
    }

    /**
     * this function returns rating of an article
     * @param $article_id is id of article,whose rating is to be return
     * @return mixed|null returns rating, if there is any to be returned
     * returns null otherwise
     */
    public function getArticleRatings($article_id){
        $article_id = $article_id + 0;
        $sql = "SELECT * FROM rating
                  WHERE rating.article_id = $article_id";

        $order = $this->db->prepare($sql);
        $order->bindParam(':article_id',$article_id);
        $order->execute();
        $executed = $order->fetchAll();;
        if($executed == null){
            $executed = null;
        }

        return $executed;
    }

    /**
     * this function returns all raters
     * @return bool|mixed|null|PDOStatement is null when there is no rater to be returned
     * returns all raters otherwise
     */
    private function getRaters(){
        $order = "SELECT * FROM user
                  WHERE user.rights = 'Recenzent'";
        $executed = $this->executeQuery($order);
        $executed = $this->resultObjectToArray($executed);
        return $executed;
    }

    /**
     * this returns all users as options
     * @return string is command, which creates option for selectbox
     */
    public function getUserOptions(){
        $users = $this->getRaters();
        $options = "";
        foreach ($users as $key){
            $options .= "<option value='$key[nick]'>$key[nick]</option>";
        }
        return $options;
    }

    /**
     * this creates rating for raters
     * @param $article article to be rated
     * @param $recenzent user who is resposible for rating article
     * @return bool|null is true, if action was succesful
     * null otherwise
     */
    public function createRating($article, $recenzent){
        $article = htmlspecialchars($article);
        $recenzent = htmlspecialchars($recenzent);
        $order = "INSERT INTO rating(user_nick,article_id)
                  VALUES (?,?)";
        $order = $this->db->prepare($order);
        $executed = $order->execute(array($recenzent, $article));
        if($executed == null){
            return null;
        } else {
            return $executed;
        }
    }

    /**
     * this function adds rating to article
     * @param $rating_id rating id, rater only changes rating which admin crated for him
     * @param $theme theme evaluation
     * @param $language language evaluation
     * @param $utility utility evaluation
     * @param $notes additional thoughts to article
     * @return null when rating couldn't been uploaded
     * returns true otherwise
     */
    public function addRating($rating_id, $theme, $language, $utility, $notes){
        $rating_id = $rating_id + 0;
        $theme = $theme + 0;
        $language = $language + 0;
        $utility = $utility + 0;
        $notes = htmlspecialchars($notes);
        $order = "UPDATE rating
                  SET theme=:theme, language=:language, impact=:utility, notes=:notes
                  WHERE rating_number = :rating_id";

        $arguments = array(':theme' => $theme, ':language' => $language, ':utility' => $utility, ':notes' => $notes, ':rating_id' => $rating_id);
        $order = $this->db->prepare($order);

        if(!($executed = $order->execute($arguments))){
            return null;
        } else {
            return $executed;
        }
    }
}
?>
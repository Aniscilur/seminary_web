<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 11:24
 */

    // creating header
    include("view/visual.php");
    getHeader("Seznam hodnocení");
?>

<?php
    // managing site control
    include("controllers/actions.class.php");
    $SiteControler = new actions();
    $SiteControler->checkActions();
?>

<h3>Seznam mně přidělených hodnocení</h3>

<?php
    if(($SiteControler->getPDOControler()->isLogged()) && ($_SESSION["user"]["rights"] == "Recenzent")){
    // displays only to logged recenzents
?>

<?php
        $ratings = $SiteControler->getPDOControler()->getArticlesToRate($_SESSION["user"]["nick"]);
        if($ratings == null){
            echo "<p id='error_display'>Databáze neobsahuje žádná nevyplněná přiřazená hodnocení!</p>";
        } else {
            echo " <!-- table with users who are not admins-->
                   <table>
                        <tr>
                            <th>Jméno článku</th>
                            <th>Kontext</th>
                            <th>Soubor</th>
                            <th>Status</th>
                        </tr>";

            foreach($ratings as $key){
                echo "<tr>
                        <td>$key[name]</td>
                        <td>$key[context]</td>
                        <td><a href='$key[file]'>Stáhnout soubor</a></td>
                        <td>";

                if($key['theme'] == NULL || $key['theme'] == 0){
                    echo "Nehodnoceno";
                } else {
                    echo "Hodnoceno";
                }

                echo "  </td>
                        <td>
                            <form method='POST' action='' id='no_bubble' >
                                <input type='hidden' name='key_id' value='$key[rating_number]'>
                                <input type='hidden' name='action' value='new_rating'>
                                <input type='submit' value='Ohodnotit'>
                            </form>
                        </td>
                      </tr>";
            }

            echo "</table>";
        }
?>

        <!-- additional links-->
        <div id="bubblelinks">
            <a href="login.php">Zpět na osobní profil</a>
        </div>
<?php
    } else {
        //displays for not logged and not recenzent
?>

    <p id='error_display'>Tyto stránky jsou přístupné pouze přihlášeným recenzentům!</p>

<?php
    }
?>

<?php
    // creating footer
    getFooter();
?>

<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 8:23
 */

    // creating header
    include("view/visual.php");
    getHeader("Nové hodnocení");
?>

<?php
    // managing site control
    include("controllers/actions.class.php");
    $SiteControler = new actions();
    $SiteControler->checkActions();
?>

    <h3>Nové hodnocení</h3>

<?php
if(($SiteControler->getPDOControler()->isLogged()) && ($_SESSION["user"]["rights"] == "Recenzent")){
    // displays only to logged recenzents
    if(isset($_COOKIE['rating'])) {
        ?>

        <!-- form with table for better alignment options-->
        <form id="article" autocomplete="off" method="post" action="">

            <!-- row contains radio with 5 options-->
            <div class="alignmenthelpers">
                <label>
                    Téma:
                </label>
            </div>

            <label for="rad01" class="labeledradio">
                <input type="radio" title="1" name="theme" value="1" id="rad01" required>
                1
            </label>
            <label for="rad02" class="labeledradio">
                <input type="radio" title="2" name="theme" value="2" id="rad02">
                2
            </label>
            <label for="rad03" class="labeledradio">
                <input type="radio" title="3" name="theme" value="3" id="rad03">
                3
            </label>
            <label for="rad04" class="labeledradio">
                <input type="radio" title="4" name="theme" value="4" id="rad04">
                4
            </label>
            <label for="rad05" class="labeledradio">
                <input type="radio" title="5" name="theme" value="5" id="rad05">
                5
            </label>
            <br>

            <!-- row contains radio with 5 options-->
            <div class="alignmenthelpers">
                <label>
                    Jazykové prostředky:
                </label>
            </div>

            <label for="rad11" class="labeledradio">
                <input type="radio" title="1" name="language" value="1" id="rad11" required>
                1
            </label>
            <label for="rad12" class="labeledradio">
                <input type="radio" title="2" name="language" value="2" id="rad12">
                2
            </label>
            <label for="rad13" class="labeledradio">
                <input type="radio" title="3" name="language" value="3" id="rad13">
                3
            </label>
            <label for="rad14" class="labeledradio">
                <input type="radio" title="4" name="language" value="4" id="rad14">
                4
            </label>
            <label for="rad15" class="labeledradio">
                <input type="radio" title="5" name="language" value="5" id="rad15">
                5
            </label>
            <br>

            <!-- row contains radio with 5 options-->
            <div class="alignmenthelpers">
                <label>
                    Přínosnost:
                </label>
            </div>

            <label for="rad21" class="labeledradio">
                <input type="radio" title="1" name="utility" value="1" id="rad21" required>
                1
            </label>
            <label for="rad22" class="labeledradio">
                <input type="radio" title="2" name="utility" value="2" id="rad22">
                2
            </label>
            <label for="rad23" class="labeledradio">
                <input type="radio" title="3" name="utility" value="3" id="rad23">
                3
            </label>
            <label for="rad24" class="labeledradio">
                <input type="radio" title="4" name="utility" value="4" id="rad24">
                4
            </label>
            <label for="rad25" class="labeledradio">
                <input type="radio" title="5" name="utility" value="5" id="rad25">
                5
            </label>

            <!-- row contains text area-->
            <div class="alignmenthelpers">
                <label for="notes">
                    Další výhrady:
                </label>
            </div>

            <textarea id="notes" name="notes" maxlength="500" rows="5" cols="50"></textarea>

            <!-- row contains send button-->
            <input type="hidden" name="action" value="add_rating">
            <input type="submit" value="Registrovat hodnocení">
        </form>

        <!-- additional links-->
        <div id="bubblelinks">
            <a href="ratingchart.php">Zpět na seznam mých hodnocení</a><br>
        </div>

<?php
        } else{
?>
            <p id='error_display'>Tyto stránky jsou silně závislé na cukru a proto vyžadují COOKIES!</p>
            <p id='error_display'>Ujistěte se, že je jejich využívání povoleno, a opakujte předešlou akci...</p>
<?php
        }
    } else {
        //displays for not logged and not recenzents
?>

    <p id='error_display'>Tyto stránky jsou přístupné pouze přihlášeným recenzentům!</p>

<?php
    }
?>

<?php
// creating footer
    getFooter();
?>
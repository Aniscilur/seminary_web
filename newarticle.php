<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 8:23
 */

    // creating header
    include("view/visual.php");
    getHeader("Nový článek");
?>

<?php
    // managing site control
    include("controllers/actions.class.php");
    $SiteControler = new actions();
    $SiteControler->checkActions();
?>

    <h3>Napsat nový příspěvek</h3>

<?php
    if(($SiteControler->getPDOControler()->isLogged()) && ($_SESSION["user"]["rights"] == "Autor")){
        // displays only to logged authors
?>

    <!-- form with table for better alignment options-->
    <form id="article" autocomplete="off" method="post" action="" enctype="multipart/form-data">

        <!-- row contains text field-->
        <div class="alignmenthelpers">
            <label for="name">
                Jméno příspěvku:
            </label>
        </div>

        <input type="text" id="name" name="name" maxlength="20" required>
        <br>

        <!-- row contains text area-->
        <div class="alignmenthelpers">
            <label for="content">
                Stručný obsah:
            </label>
        </div>

        <textarea  id="content" name="content" maxlength="500" rows="5" cols="50" required></textarea>
        <br>

        <!-- row contains filechoser with restricted to pdf-->
        <div class="alignmenthelpers">
            <label for="filepdf">
                Jméno příspěvku:
            </label>
        </div>
        <input type="file" accept="application/pdf" id="filepdf" name="filepdf" required>
        <br>

        <!-- row contains send button-->
        <input type="hidden" name="action" value="add_article">
        <input type="submit" value="Registrovat článek">
    </form>

    <div id="bubblelinks">
        <a href="login.php">Zpět na osobní profil</a><br>
    </div>

<?php
    } else {
        //displays for not logged and not authors
?>

        <p id='error_display'>Tyto stránky jsou přístupné pouze přihlášeným autorům!</p>

<?php
    }
?>


<?php
    // creating footer
    getFooter();
?>
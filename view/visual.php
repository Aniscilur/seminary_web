<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 8. 11. 2018
 * Time: 8:13
 */

/**
 * returns code creating header
 * @param string $title
 */
function getHeader($title=""){
?>
    <!DOCTYPE html>
    <html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <title>Gamer's Paradise - <?php echo $title; ?></title>
        <link rel="stylesheet" href="view/style.css" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <!-- header contains only option bar and headings-->
    <div id="header">
        <div id="optionbar"></div>
        <h1>Gamer's Paradise</h1>
        <h2>Konference o hrách</h2>
    </div>

    <!-- menu created by lining links in list-->
    <div id="menu">
        <ul>
            <li>
                <a href="index.php" title="Domů">Úvodní stránka</a>
            </li>
            <li>
                <a href="articles.php" title="Novinky">Novinky</a>
            </li>
            <li>
                <a href="login.php" title="Log in">Přihlášení</a>
            </li>
            <li>
                <a href="register.php" title="Vytvořit účet">Registrace</a>
            </li>
        </ul>
    </div>

    <!-- this part contains the real content of the side-->
    <div id="announcement">

<?php
}

/**
 * returns code creating footer
 */
function getFooter(){
?>
        </div>

        <!-- footer contains only author and year as simple text-->
        <div id="footer">
            &copy; Aniscilur, 2018
        </div>
        </body>
    </html>
<?php
}
?>
